var now = Date.now();

function transform() {
  $('span:not(.ag-editable, .sr-only), p:not(.ag-editable), div:not(.ag-editable), h1:not(.ag-editable), h2:not(.ag-editable), h3:not(.ag-editable), h4:not(.ag-editable), h5:not(.ag-editable), h6:not(.ag-editable), a:not(.ag-editable)')
  .each(function(i, e){
    var element = $(this);
    
    if(element.children().length === 0 && element.text().length > 0)
      element.addClass('ag-editable');
  });
}

function apos(fnct, type, options, id, key) {
	var result;
  
    var uniq = CryptoJS.MD5(key + now + id);

	result = "{{ " 
		+ fnct 
		+ "(" + type + ", 'ag" + uniq + id + "', {";

	for(var i = 0; i < options.length; i++) {
		result += options[i];

		if(i < options.length - 1)
			result += ','
	}

	result += "})"
		+ " }}";

	return result;
}

function cssgen(key) {
  $('link').each(function(i, e){
    var element = $(this);
    var href =  element.attr('href');
    
    if(typeof href !== typeof undefined && href.substring(0,4) != 'http')
      element.attr('href', '/templates/' + key + '/' + href);
    
  });
}

function jsgen(key) {
  $('script').each(function(i, e){
    var element = $(this);
    var src =  element.attr('src');
    
    if(typeof src !== typeof undefined && src.substring(0,4) != 'http')
      element.attr('src', '/templates/' + key + '/' + src);
  });
}

function imggen(key) {
  $('img').each(function(i, e){
    var element = $(this);
    var src =  element.attr('src');
    
    if(typeof src !== typeof undefined && src.substring(0,4) != 'http')
      element.attr('src', '/templates/' + key + '/' + src);
  });
}

function aposgen(key) {
  var n = 0;
	
  $('.ag-editable').each(function(i, e){
      var element = $(this);
      var parent = element.parent();

      var fnct = element.attr('ag-function');
      var type = element.attr('ag-type');
    
      fnct = typeof fnct === 'undefined' || fnct.length === 0 ? 'aposArea': fnct;
      type = typeof type === 'undefined' || type.length === 0 ? 'page': type;
      
      var specs = {
        uinit: { attr: 'ag-uinit', value: false },
        minimal : { attr: 'ag-minimal', value: false }
      };
    
      specs.uinit.value = element.is('[' + specs.uinit.attr + ']') ? !specs.uinit.value : specs.uinit.value;
      specs.minimal.value = element.is('[' + specs.minimal.attr + ']') ? !specs.minimal.value : specs.minimal.value;

      var options = [];
      
      if(!specs.uinit.value && !specs.uinit.minimal && element.text().length > 0 && /\S/.test(element.text()))
        options.push('initialContent: "' + element.text() + '"');
      
      var check = {
        controls: false,
        styles: false,
        lockups: false
      };

      var willBeRemoved = [];
    
      if(specs.uinit.value) willBeRemoved.push(specs.uinit.attr);
      if(specs.minimal.value) willBeRemoved.push(specs.minimal.attr);

      $.each(this.attributes, function(i, attr){
        if(attr.name.indexOf('ag-a-') > -1) {
          var split = attr.name.split('-');
          var focus = split[split.length - 1];

          var opt = focus + ': ' + attr.value;
          options.push(opt);

          if(attr.name.indexOf('ag-a-controls') > -1)
            check.controls = true;

          if(attr.name.indexOf('ag-a-styles') > -1)
            check.styles = true;

          if(attr.name.indexOf('ag-a-lockups') > -1)
            check.lockups = true;
        }

        if(attr.name.indexOf('ag-') > -1) {
          willBeRemoved.push(attr.name);
        }
      });

      $.each(willBeRemoved, function(i, attr){
        element.removeAttr(attr);
      });

      element.removeClass('ag-editable');
      

      if(!check.controls && !specs.minimal.value) {
        if(!check.styles)
          options.push("controls: ['bold', 'italic', 'createLink', 'unlink', 'slideshow', 'video']");
        else
          options.push("controls: ['style', 'bold', 'italic', 'createLink', 'unlink', 'slideshow', 'video']");
      }

      if(!check.lockups && !specs.minimal.value)
        options.push("lockups: ['left', 'right']");

      var aposContent = apos(fnct, type, options, n, key);

      element.html(aposContent)

      n++;
  });
}

$(function(){
  
  var key = $('#apos-content').attr('data-key');
  $('#apos-content').remove();
  
  //transform();
  cssgen(key);
  imggen(key);
  jsgen(key);
  aposgen(key);

  var code = '<html>' + $('html').html() + '</html>';
  
  var html = {
    head: '',
    body: '',
    end: ''
  };

  $('head > link, head > script').each(function(i, e){
    html.head += $(this)[0].outerHTML;
  });
  $('body > script').each(function(i, e){
    html.end += $(this)[0].outerHTML;
    $(this).remove();
  });
  html.body = $('body').html();
  
  $.post('/template/save', { key: key, code: code, name: 'aposgen.html', content: html }, function(res){
    console.log(res);
  });
});