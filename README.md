# AposGen

Lancer un **sails lift** dans le dossier

Rajouter la classe **ag-editable** a n'importe quel élément qui doit être éditable dans l'index.html

Mettre le fichier index.html du template dans /templates/[NOM]/index.html
Le [NOM] doit correspondre au nom du dossier du template dans l'application principale 
(Example: aposgen -> /templates/test/index.html | cms -> /public/templates/test)

Aller sur localhost:1337/gen/[NOM]
(Example: http://localhost:1337/gen/test)

Le fichier aposgen.html est directement créé au même endroit que le fichier index.html

Les liens vers les ressources du templates sont automatiquement mises à jour !

Copier tout ce qu'il y a dans la balise Body dans le block main du fichier /views/pages/home.html (sur le cms)
Copier les fichiers CSS et JS du head dans le head du fichier /views/global/base.html
Copier les scripts en bas de page, dans /views/global/base.html avant les lignes :

{# Must be present in the page in order to use apos's widget editors, admin dialogs, etc. #}
{{ aposTemplates(when) }}

C'est FINI !	
	

a [Sails](http://sailsjs.org) application