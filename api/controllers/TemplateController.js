/**
 * TemplateController
 *
 * @description :: Server-side logic for managing templates
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	


  /**
   * `TemplateController.generate()`
   */
  generate: function (req, res) {
		
		var key = req.param('key');
		
		var fs = require('fs');
		var path = require('path');
		
		var filePath = path.join(sails.config.appPath, path.join('templates', path.join(key, 'index.html')));
		
		fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
			
			data += '<div id="apos-content" data-key="' + key + '">'
                + '<script src="/js/dependencies/sails.io.js"></script>'
                + '<script src="/js/vendors/jquery.min.js"></script>'
                + '<script src="/js/md5.js"></script>'
				+ '<script src="/js/generate.js"></script>'
                + '</div>';
			
		 	res.writeHead(200, {'Content-Type': 'text/html'});
			res.write(data);
			res.end();
		});
  },
	
	/**
   * `TemplateController.save()`
   */
  save: function (req, res) {
    var key = req.param('key');
    var code = req.param('code');
    var name = req.param('name');
    var content = req.param('content');

    var fs = require('fs');
    var path = require('path');

    var baseFile = path.join(sails.config.appPath, path.join('bases', 'base.html'));
    var homeFile = path.join(sails.config.appPath, path.join('bases', 'home.html'));

    var dir = path.join(sails.config.appPath, path.join('templates', key));
    //var filePath = path.join(dir, name);

    var aposFolder = path.join(dir, 'aposgen');
    if (!fs.existsSync(aposFolder)) fs.mkdirSync(aposFolder);

    var baseContent = fs.readFileSync(baseFile, 'utf8');
    var homeContent = fs.readFileSync(homeFile, 'utf8');
    
    var baseUpdated = baseContent.replace(/!%aposgenhead%!/g, content.head);
    baseUpdated = baseUpdated.replace(/!%aposgenend%!/g, content.end);
    
    console.log(content.body);
    
    var homeUpdated = homeContent.replace(/!%aposgenbody%!/g, content.body);
    
    var basePath = path.join(aposFolder, 'base.html');
    var homePath = path.join(aposFolder, 'home.html');
    
    if(fs.existsSync(basePath)) fs.unlinkSync(basePath);
    if(fs.existsSync(homePath)) fs.unlinkSync(homePath);
    
    fs.writeFileSync(basePath, baseUpdated, 'utf8');
    fs.writeFileSync(homePath, homeUpdated, 'utf8');
    
    return res.json('Success');
    

    /*if(fs.existsSync(filePath)) {
      fs.unlinkSync(filePath);
    }

    fs.writeFile(filePath, code, function(err) {
        return res.json('Saved file as ' + filePath);
    });*/
  }
};

